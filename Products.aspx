﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Products.aspx.cs" Inherits="Products" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Chapter 14: Northwind</title>
    <link href="Main.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <header>
        &nbsp;<img alt="Northwind Solutions" src="Images/Northwind.jpg" /><br />
    </header>
    <section>
    <form id="form1" runat="server">
        &nbsp;<br />
        
        <asp:GridView ID="gvProduct" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" BackColor="White" BorderColor="#E7E7FF" BorderStyle="None" BorderWidth="1px" CellPadding="3" DataKeyNames="ProductID" DataSourceID="sqldsProducts" GridLines="Horizontal" HorizontalAlign="Center" OnRowUpdated="gvProduct_RowUpdated" PageSize="15">
            <AlternatingRowStyle BackColor="#F7F7F7" />
            <Columns>
                <asp:BoundField DataField="ProductID" HeaderText="ID" InsertVisible="False" ReadOnly="True" SortExpression="ProductID">
                <HeaderStyle HorizontalAlign="Center" Width="50px" />
                <ItemStyle HorizontalAlign="Center" />
                </asp:BoundField>
                <asp:BoundField DataField="ProductName" HeaderText="Name" SortExpression="ProductName">
                <HeaderStyle HorizontalAlign="Center" Width="250px" />
                <ItemStyle HorizontalAlign="Center" />
                </asp:BoundField>
                <asp:TemplateField HeaderText="Units On Hand" SortExpression="UnitsInStock">
                    <EditItemTemplate>
                        <asp:TextBox ID="tbItemsOnHand" runat="server" Text='<%# Bind("UnitsInStock") %>'></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvItemsOnHand" runat="server" ControlToValidate="tbItemsOnHand" CssClass="error" Display="Dynamic" ErrorMessage="This field is required to be filled out.">*</asp:RequiredFieldValidator>
                        <asp:CompareValidator ID="cvItemsOnHand" runat="server" ControlToValidate="tbItemsOnHand" CssClass="error" Display="Dynamic" ErrorMessage="Please enter valid number greater than 0." Operator="GreaterThanEqual" Type="Integer" ValueToCompare="0">*</asp:CompareValidator>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# Bind("UnitsInStock") %>'></asp:Label>
                    </ItemTemplate>
                    <HeaderStyle HorizontalAlign="Center" />
                    <ItemStyle HorizontalAlign="Center" Width="100px" />
                </asp:TemplateField>
                <asp:CommandField ShowEditButton="True">
                <HeaderStyle HorizontalAlign="Center" Width="50px" />
                <ItemStyle HorizontalAlign="Center" Width="55px" />
                </asp:CommandField>
            </Columns>
            <FooterStyle BackColor="#B5C7DE" ForeColor="#4A3C8C" />
            <HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#F7F7F7" />
            <PagerStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" HorizontalAlign="Right" />
            <RowStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" />
            <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="#F7F7F7" />
            <SortedAscendingCellStyle BackColor="#F4F4FD" />
            <SortedAscendingHeaderStyle BackColor="#5A4C9D" />
            <SortedDescendingCellStyle BackColor="#D8D8F0" />
            <SortedDescendingHeaderStyle BackColor="#3E3277" />
        </asp:GridView>
        
        <asp:SqlDataSource ID="sqldsProducts" runat="server" ConnectionString="<%$ ConnectionStrings:ProductsConnectionString %>" ProviderName="<%$ ConnectionStrings:ProductsConnectionString.ProviderName %>" SelectCommand="SELECT [ProductID], [ProductName], [UnitsInStock] FROM [tblProducts]" UpdateCommand="UPDATE [tblProducts] SET [ProductName] = ?, [UnitsInStock] = ? WHERE [ProductID] = ?"></asp:SqlDataSource>
        <asp:ValidationSummary ID="vsItemsOnHand" runat="server" CssClass="error" />
        <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
    </form>
    </section>
</body>
</html>
